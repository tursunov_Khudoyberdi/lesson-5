// 1
const countVowels = str =>
  Array.from(str).filter(letter => "aeiouAEIOU".includes(letter)).length;
console.log(countVowels("The Quick Brown Fox IO"));

// 2
const startDate = "2021-10-17";
const endDate = "2022-01-01";

const diffInMs = new Date(endDate) - new Date(startDate);
const diffInDays = diffInMs / (1000 * 60 * 60 * 24);

console.log(diffInDays);

// 3
const arrSymbols = arr => arr.filter(el => typeof el === "symbol").length;
console.log(
  arrSymbols([
    345,
    Symbol("*"),
    34,
    "hi",
    { name: "Ryuk" },
    Symbol("@"),
    Symbol("$"),
    "man",
  ]),
);

// 4
const intersection = (arr1, arr2) => arr1.filter(val => arr2.includes(val));
console.log(intersection([1, 2, 3], [2, 3, 4, 5]));

// 5
setTimeout(() => alert("I should be alerted after 4 seconds"), 4000);

// 6
const summer = arr =>
  Array.from(arr.filter(el => typeof el === "number")).reduce(
    (prev, cur) => prev + cur,
    0,
  );
console.log(summer([3, 7, 1.5, 2.5, 1, "hi"]));

// 7
const specChar = (char, str) =>
  str.split("").reduce((acc, ch) => (ch === char ? acc + 1 : acc), 0);
console.log(specChar("I", "Hi, Bro, I think that you are making progress"));
